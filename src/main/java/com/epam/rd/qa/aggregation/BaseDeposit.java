package com.epam.rd.qa.aggregation;

import java.math.BigDecimal;
import java.math.RoundingMode;
import org.junit.jupiter.api.io.TempDir;

public class BaseDeposit extends Deposit{
    public BaseDeposit(BigDecimal amount, int period) {
        super(amount, period);
    }

    @Override
    public BigDecimal income() {
        int newPeriod = getPeriod();
        double newAmount = Double.parseDouble(getAmount().toString());
        double totalAmount = newAmount;
        int i = 1;
        while (i <= newPeriod) {
            totalAmount *= 1.05;
            i++;
        }
        double profit = totalAmount - newAmount;
        BigDecimal bd = new BigDecimal(Double.toString(profit));
        bd = bd.setScale(2, RoundingMode.HALF_DOWN);
        return bd;
    }
}
