package com.epam.rd.qa.aggregation;

import static org.mockito.ArgumentMatchers.doubleThat;
import static org.mockito.ArgumentMatchers.intThat;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class LongDeposit extends Deposit{

    public LongDeposit(BigDecimal depositAmount, int depositPeriod) {
        super(depositAmount, depositPeriod);
    }
    @Override
    public BigDecimal income() {
        int newPeriod = getPeriod();
        double newAmount = Double.parseDouble(getAmount().toString());
        double totalAmount = newAmount;
        int i = 1;
        while (i <= newPeriod) {
            if (i<=6) {
                i++;
                continue;
            }
            totalAmount *= 1.15;
            i++;
        }
        double profit = totalAmount - newAmount;
        BigDecimal bd = new BigDecimal(Double.toString(profit));
        bd = bd.setScale(2, RoundingMode.HALF_DOWN);
        return bd;
    }


}
