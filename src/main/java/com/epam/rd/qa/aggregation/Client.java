package com.epam.rd.qa.aggregation;

import static org.mockito.ArgumentMatchers.doubleThat;
import static org.mockito.Mockito.RETURNS_SMART_NULLS;

import java.math.BigDecimal;

public class Client {
    private Deposit[] deposits;


    public Client() {
        deposits = new Deposit[10];
    }

    public boolean addDeposit(Deposit deposit) {
        boolean isEmpty = false;
        for (int i = 0; i < deposits.length; i++) {
            if (deposits[i]==null) {
                deposits[i] = deposit;
                isEmpty = true;
            }
        }
        return isEmpty;
    }

    public BigDecimal totalIncome() {
        double total_income = 0;
        for (Deposit deposit : deposits) {
            total_income+= Double.parseDouble(deposit.income().toString());
        }
        BigDecimal incomeBD = new BigDecimal(total_income);
        return incomeBD;
    }

    public BigDecimal maxIncome() {
        double max_income = 0;
        for (Deposit deposit : deposits) {
            if (max_income<Double.parseDouble(deposit.income().toString())){
                max_income = Double.parseDouble(deposit.income().toString());
            }
        }
        BigDecimal max_incomeBD = new BigDecimal(max_income);
        return max_incomeBD;
    }

    public BigDecimal getIncomeByNumber(int number) {
        BigDecimal defaultBD = new BigDecimal(0);
        for (int i = 0; i < deposits.length; i++) {
            if (i == number) {
                BigDecimal incomeBD = deposits[i].income();
                return incomeBD;
            }
        }
        return defaultBD;
    }
}
