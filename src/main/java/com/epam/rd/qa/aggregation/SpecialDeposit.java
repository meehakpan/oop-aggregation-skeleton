package com.epam.rd.qa.aggregation;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class SpecialDeposit extends Deposit{

    public SpecialDeposit(BigDecimal amount, int period) {
        super(amount, period);
    }

    @Override
    public BigDecimal income() {
        int newPeriod = getPeriod();
        double newAmount = Double.parseDouble(getAmount().toString());
        double totalAmount = newAmount;
        double i = 1;
        while (i <= newPeriod) {
            double percentage = 1.00 + i/100;
            totalAmount *= percentage;
            i++;
        }
        double profit = totalAmount - newAmount;
        BigDecimal bd = new BigDecimal(Double.toString(profit));
        bd = bd.setScale(2, RoundingMode.HALF_DOWN);
        return bd;
    }

}

