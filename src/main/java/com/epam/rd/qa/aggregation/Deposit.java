package com.epam.rd.qa.aggregation;
import static org.mockito.ArgumentMatchers.doubleThat;
import static org.mockito.ArgumentMatchers.intThat;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Iterator;

public abstract class Deposit{
    public final BigDecimal amount;
    public final int period;
    public Deposit (BigDecimal depositAmount, int depositPeriod) {
        if (Double.parseDouble(depositAmount.toString()) <0 ) {
            throw new IllegalArgumentException();
        }else {
            amount = depositAmount;
        }
        if (depositPeriod<0) {
            throw new IllegalArgumentException();
        }else {
            period = depositPeriod;
        }
    }
    public BigDecimal getAmount() {
        return amount;
    }
    public int getPeriod() {
        return period;
    }
    public abstract BigDecimal income();
}
